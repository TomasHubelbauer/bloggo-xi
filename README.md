# Xi

- [Xi](https://github.com/google/xi-editor)
- [Xi Windows](https://github.com/google/xi-win)
- [Xi Mac](https://github.com/google/xi-mac)

The Mac frontend is the most complete it seems, could be used for hints about the protocol.

There are also protocol notes in [frontend.md](https://github.com/google/xi-editor/blob/master/doc/frontend.md).

My contributions to Xi-Win:

- [Implement *Save* and *Save As…*](https://github.com/google/xi-win/pull/12)
